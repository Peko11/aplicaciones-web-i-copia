function meses() {
  let months = new Array(
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre"
  );
  for (let i = 0; i < months.length; i++) {
    alert("Mes: " + months[i]);
  }
}

class Producto_alimenticio {
  constructor(code, name, price) {
    this.code = code;
    this.name = name;
    this.price = price;
  }
  get print() {
    //accedemos al imprimeDatos a traves de un método getter
    return this.imprimeDatos();
  }
  imprimeDatos() {
    document.write(
      "<div class='resultado'> Código: " +
        this.code +
        "<br> Nombre: " +
        this.name +
        "<br> Precio: " +
        this.price + "</div>"
    );
  }
}

let listProduct = new Array( //instanciamos la clase y la guardamos directamente en el array
  new Producto_alimenticio("P01", "Leche", 1),
  new Producto_alimenticio("P02", "arroz", 0.5),
  new Producto_alimenticio("P03", "carne", 3)
);
for (let index = 0; index < listProduct.length; index++) {
  //recorremos el array
  console.log(listProduct[index].print); //imprimimos llamando al metodo getter que llamará a imprimeDatos
}
